#!/usr/bin/perl

use LWP;
use XML::LibXML;
use strict;
use warnings;
use DateTime;

my $EMAIL_TO = '';
my $EMAIL_FROM = '';
my $SENDMAIL = '/usr/sbin/sendmail';
my $MAX_RESULTS = 32;
my $DAYS_HISTORY = 1;
my $MAX_REQUESTS = 6;
my $TRUE = 1;
my $FALSE = 0;

my $output = "";

my $newestDate = '';

my $lastDate = DateTime->now(time_zone => 'Z')->subtract(days => $DAYS_HISTORY);

sub BuildUrl
{
   my @categories = ('cs.os', 'cs.ar', 'cs.dc', 'cs.ET', 'cs.SE', 'cs.NI');
   my $searchQuery = 'search_query=';
   for my $i (0..$#categories)
   {
      $searchQuery .= $categories[$i];
      if ($i != $#categories)
      {
         $searchQuery .= "+OR+"
      }
   }
   my $url = "http://export.arxiv.org/api/query?$searchQuery&start=$_[0]&max_results=$MAX_RESULTS&sortBy=lastUpdatedDate&sortOrder=descending";
   print "URL: $url\n";
   return $url
}

sub ParseDateZ
{
   my ($y, $m, $d, $h, $min, $s) = $_[0] =~ /(\d\d\d\d)\-(\d\d)\-(\d\d)T(\d\d):(\d\d):(\d\d)Z/ or die;
   return DateTime->new(
      year      => $y,
      month     => $m,
      day       => $d, 
      hour      => $h,
      minute    => $min,
      second    => $s,
      time_zone => 'Z'
      );
}

sub HandleContent
{
   my $root_element =  XML::LibXML->load_xml(string => $_[0]->content());
   my $entryCount = 0;
   foreach my $entry ($root_element->findnodes("//*[local-name() = 'feed']/*[local-name() = 'entry']"))
   {
      my $updated = $entry->getChildrenByTagName("updated")->to_literal;
      if($newestDate eq "")
      {
         $newestDate = $updated;
      }
      my $updatedDateTime = ParseDateZ($updated);
      if( ($updatedDateTime->subtract_datetime($lastDate))->is_negative() or ($updatedDateTime->subtract_datetime($lastDate))->is_zero())
      {
         last;
      }
      $output .= "<P>\n";
      my $title = Encode::encode_utf8($entry->getChildrenByTagName("title")->to_literal);
      while ($title =~ s/\R//g) {}
      
      my $id = $entry->getChildrenByTagName("id")->to_literal;
      while ($id =~ s/\R//g) {}
      
      $output .= "<B><A href=\"$id\">$title</A></B>\n<BR>\n";

      

      my $authorLine = '';
      foreach my $author  ($entry->getChildrenByTagName("author"))
      {
         my $authorName = Encode::encode_utf8($author->to_literal);
         while ($authorName =~ s/^\s+|\s+$//g) {}
         $authorLine .= $authorName . ', ';
      }
      
      $authorLine =~ s/^(.*?), $/$1\n/;

      $output .= "\n$authorLine<BR>\n$updated<BR>";

      $entryCount += 1
   }

   $output .= "</P>\n";
   return $entryCount;
}

if(open(my $fh, '<', '~/.arxiv.dat'))
{
   my $lastDateStr = readline $fh;
   if(defined $lastDateStr)
   {
      while ($lastDateStr =~ s/^\s+|\s+$//g) {}
      close $fh;
      $lastDate = ParseDateZ($lastDateStr);
   }
}

my $browser = LWP::UserAgent->new();
my $processed = $MAX_RESULTS;
my $totalProcessed = 0;
my $count = 0;
while($count < $MAX_REQUESTS)
{
   my $start = $count * $MAX_RESULTS;
   $processed = HandleContent($browser->get(BuildUrl($start)), $count);
   $totalProcessed += $processed;
   print "Processed $processed\n";
   last if($processed != $MAX_RESULTS);
   $count += 1;
   sleep 3;
}

print "Completed Processing\n";

if( $totalProcessed > 0)
{
   my $outFile = "/tmp/arxiv_mail_" . DateTime->now(time_zone => 'Z');
   open(my $fh, '>', $outFile) or die;
   print $fh "To: $EMAIL_TO\n";
   print $fh "From: $EMAIL_TO\n";
   print $fh "Subject: " . "arXiv Update for " . DateTime->now(time_zone => 'Z') . "\n";
   print $fh "Content-type: text/html\n";
   print $fh $output;
   close $fh;
   
   print `sendmail $EMAIL_TO < $outFile`;
}

if(open(my $fh, '>', '~/.arxiv.dat'))
{
   print $fh $newestDate;
   close $fh;
}

